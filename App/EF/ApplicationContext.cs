﻿using App.Models;
using Microsoft.EntityFrameworkCore;

namespace App.EF;

public sealed class ApplicationContext : DbContext
{
    private readonly IConfiguration _configuration;
    public DbSet<User>? Users { get; set; }

    public ApplicationContext(DbContextOptions<ApplicationContext> options, IConfiguration configuration)
        : base(options)
    {
        _configuration = configuration;
        Database.EnsureCreated(); // создаем базу данных при первом обращении
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);
        var connectionString = _configuration.GetConnectionString("DefaultConnection");

        if (connectionString == null)
        {
            throw new InvalidOperationException("no connection");
        }

        optionsBuilder.UseMySQL(connectionString);
    }
}